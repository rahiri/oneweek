﻿using UnityEngine;
using System.Collections;

public class Referee : MonoBehaviour
{

    public GameObject HomeReferee;
    public GameObject FirstBaseReferee;
    public GameObject SecondBaseReferee;
    public GameObject ThirdBaseReferee;

    //Dot Product result Values are between -1 to 1.
    private float FoulDegree = 0.0f;

    private Vector3 CenterVector = Vector3.zero;
    private float BaseMagnitude = 1.0f;

    public enum Hit_State
    {
        HIT_STATE_DEFAULT = 0,
        HIT_STATE_FAIR,
        HIT_STATE_FOUL,
    }

    void Start()
    {
        CenterVector = SecondBaseReferee.transform.position - HomeReferee.transform.position;
        CenterVector.Normalize();

        Vector3 thirdBaseCornerVector = ThirdBaseReferee.transform.position - HomeReferee.transform.position;
        BaseMagnitude = thirdBaseCornerVector.magnitude;
        thirdBaseCornerVector.Normalize();

        /*
        Vector3 FirstBaseCornerVector = FirstBaseReferee.transform.position - HomeReferee.transform.position;
        FirstBaseCornerVector.Normalize();
        */

        FoulDegree = Vector3.Dot(CenterVector, thirdBaseCornerVector);


    }

    public Hit_State RequestHitCall(Transform ballTransform)
    {
        Hit_State returnHitState = Hit_State.HIT_STATE_DEFAULT;

        Vector3 homeToBallVector = ballTransform.position - HomeReferee.transform.position;
        homeToBallVector.Normalize();

        float testFoulDegree = Vector3.Dot(CenterVector, homeToBallVector);

        if(testFoulDegree < FoulDegree)
        {
            returnHitState = Hit_State.HIT_STATE_FOUL;
        }
        else
        {
            returnHitState = Hit_State.HIT_STATE_FAIR;
        }

        return returnHitState;
    }

    public bool IsPastBase( Transform ballTransform)
    {
        Vector3 homeToBallVector = ballTransform.position - HomeReferee.transform.position;
        float magnitude = homeToBallVector.magnitude;

        return magnitude > BaseMagnitude;
    }

    public float GetBallMagnitude(Transform ballTransform)
    {
        Vector3 homeToBallVector = ballTransform.position - HomeReferee.transform.position;
        return homeToBallVector.magnitude;

    }
}