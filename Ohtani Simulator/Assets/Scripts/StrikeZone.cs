﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrikeZone : MonoBehaviour
{
    public GameObject StrikeZoneBallObject;

    public uint DifficultyGridWidth = 1;

    public uint DifficultyGridHeight = 1;

    private Vector3 TargetPosition;

    private List<int> EasyIndexes = new List<int>(new int[] {3,4,5});
    private List<int> NormalIndexes = new List<int>(new int[] { 1, 3, 4, 5, 7});
    private List<int> HardIndexes = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });

    private List<Vector3[]> Grid;

    //private Rigidbody RigidbodyComponent;

    // Use this for initialization
    void Start ()
    {
        Reset();
        CreateDifficultyGrid();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //MovedToTarget();
	}

    public void Reset()
    {
        //RigidbodyComponent = GetComponent<Rigidbody>();
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Testing StrikeZone entered");
        Vector3 collisionPoint = TargetPosition;

        //Put a StaticBall Image on collisionPoint
        AddStaticBallImageToPosition(collisionPoint);
    }

    private void AddStaticBallImageToPosition(Vector3 position)
    {
        StrikeZoneBallObject.transform.position = position;
        StrikeZoneBallObject.SetActive(true);
    }

    public void ClearStaticBallImage()
    {
        StrikeZoneBallObject.SetActive(false);
    }

    public Vector3 GetTargetPosition(Difficulty.Modes diffMode)
    {
        Vector3 returnVector3 = Vector3.zero;
        int randomSquareIndex = 0;
        int randomSquare = 0;

        switch (diffMode)
        {
            case Difficulty.Modes.DIFFICULTY_MODE_EASY:
                {
                    randomSquareIndex = Random.Range(0, EasyIndexes.Count);
                    randomSquare = EasyIndexes[randomSquareIndex];
                }
                break;
            case Difficulty.Modes.DIFFICULTY_MODE_MEDIUM:
                {
                    randomSquareIndex = Random.Range(0, NormalIndexes.Count);
                    randomSquare = NormalIndexes[randomSquareIndex];
                }
                break;
            case Difficulty.Modes.DIFFICULTY_MODE_HARD:
                {
                    randomSquareIndex = Random.Range(0, HardIndexes.Count);
                    randomSquare = HardIndexes[randomSquareIndex];
                }
                break;
        };


        float randomPosY = Random.Range(Grid[randomSquare][0].y, Grid[randomSquare][1].y);
        float randomPosZ = Random.Range(Grid[randomSquare][0].z, Grid[randomSquare][1].z);

        returnVector3.x = Grid[randomSquare][0].x;
        returnVector3.y = randomPosY;
        returnVector3.z = randomPosZ;

        TargetPosition = returnVector3;

        return returnVector3;

    }

    private void CreateDifficultyGrid()
    {
        float Width = 1.0f * transform.localScale.z;
        float Height = 1.0f * transform.localScale.y;

        Vector3 ToCorner = Vector3.zero;
        ToCorner.y = Height / 2.0f;
        ToCorner.z = -Width / 2.0f;
        Vector3 topLeftCorner = transform.position + ToCorner;

        Grid = new List<Vector3[]>();

        float WidthFactor = Width / DifficultyGridWidth;
        float HeightFactor = Height / DifficultyGridHeight;

        Vector3 ToDownRightCorner = Vector3.zero;
        ToDownRightCorner.z = WidthFactor;
        ToDownRightCorner.y = -HeightFactor;

        for (int gridHeightIndex = 0; gridHeightIndex < DifficultyGridHeight; ++gridHeightIndex)
        {
            for (int gridWidthIndex = 0; gridWidthIndex < DifficultyGridWidth; ++gridWidthIndex)
            {
                Vector3[] subSectionTopLeftDownRightPoints = new Vector3[2];
                subSectionTopLeftDownRightPoints[0] = topLeftCorner; //TopLeft
                subSectionTopLeftDownRightPoints[0].z += gridWidthIndex * WidthFactor;
                subSectionTopLeftDownRightPoints[0].y -= gridHeightIndex * HeightFactor;

                subSectionTopLeftDownRightPoints[1] = topLeftCorner + ToDownRightCorner; //DownRight
                subSectionTopLeftDownRightPoints[1].z += gridWidthIndex * WidthFactor;
                subSectionTopLeftDownRightPoints[1].y -= gridHeightIndex * HeightFactor;
                Grid.Add(subSectionTopLeftDownRightPoints);
            }
        }

        //Testing
        //RenderGrid();

    }

    private void RenderGrid()
    {
        for (int gridIndex = 0; gridIndex < Grid.Count; ++gridIndex)
        {
            AddExtraObjectAt(Grid[gridIndex][0]);
            AddExtraObjectAt(Grid[gridIndex][1]);
        }
    }

    private void AddExtraObjectAt(Vector3 position)
    {
        GameObject newObject = GameObject.Instantiate(StrikeZoneBallObject);
        newObject.SetActive(true);
        newObject.transform.position = position;
    }
}
