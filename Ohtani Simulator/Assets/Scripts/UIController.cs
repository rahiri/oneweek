﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text ResultText;

    public void SetText(string text)
    {
        ResultText.text = text;
    }

    public void ToggleResultText(bool on)
    {
        ResultText.enabled = on;
    }

}