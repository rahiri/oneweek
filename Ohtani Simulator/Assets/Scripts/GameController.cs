﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public PlayerController PlayerController;

    public Referee Referee;
    public Pitcher Pitcher;
    public Bat Bat;


    public Camera MainCamera;
    public Camera OnHitCamera;

    public UIController UIController;

    private float scoreTimer = 0.0f;
    private float strikeTimer = 0.0f;
    private float endStateTimer = 0.0f;

    private float batterOutTimer = 0.0f;
    private bool StartBatterOutTimer = false;

    public enum GameStates
    {
        Unknown,
        PitchState,
        BatState,
        StrikeState,
        ScoreState,
        EndState,
    }

    [HideInInspector]
    //Public for FPS
    public GameStates CurrentState;
    private GameStates PreviousCurrentState;


    private bool BallHit;
    private GameObject Ball;

    private Referee.Hit_State EnterHitStatus = Referee.Hit_State.HIT_STATE_DEFAULT;
    private Referee.Hit_State RollBallStatus = Referee.Hit_State.HIT_STATE_DEFAULT;

    private bool SecondEvaluationCompleted = false;

    private bool FairScore = true;
    private int  NewScore = 0;

    private int TotalScore = 0;

    private int Strikes = 0;
    private int Outs = 0;


    // Use this for initialization
    void Start ()
    {
        PreviousCurrentState = GameStates.Unknown;
        CurrentState = GameStates.PitchState;
        BallHit = false;
        Bat.OnBallHit += Bat_OnBallHit;
        Ball = null;
	}

    private void Reset()
    {
        PreviousCurrentState = GameStates.Unknown;
        CurrentState = GameStates.PitchState;
        BallHit = false;
        Bat.OnBallHit += Bat_OnBallHit;
        Ball = null;
        
        NewScore = 0;
        TotalScore = 0;
        Strikes = 0;
        Outs = 0;
}

    private void Bat_OnBallHit(GameObject ball)
    {
        if (CurrentState == GameStates.PitchState)
        {
            BallHit = true;
            OnHitCamera.GetComponent<OnHitCamera>().SetTargetToFollow(ball);
            Ball = ball;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if(CurrentState != PreviousCurrentState)
        {
            PreviousCurrentState = CurrentState;
            StartNewState();
        }

        switch(CurrentState)
        {
            case GameStates.PitchState:
                HandlePitchState();
                break;
            case GameStates.BatState:
                HandleBatState();
                break;
            case GameStates.StrikeState:
                HandleStrikeState();
                break;
            case GameStates.ScoreState:
                HandleScoreState();
                break;
            case GameStates.EndState:
                HandleEndState();
                break;
        };

    }

    void HandlePitchState()
    {
        //set things to start pitching
        Pitcher.AllowPitch = true;
        PlayerController.AllowInput = true;

        //check to see if bat hit
        if (BallHit)
        {
            CurrentState = GameStates.BatState;
            BallHit = false;
            Pitcher.AllowPitch = false;
            PlayerController.AllowInput = false;
        }
        else
        {
            if (Pitcher.HasCatcherCaughtBall)
            {
                CurrentState = GameStates.StrikeState;
                BallHit = false;
                Pitcher.AllowPitch = false;
                PlayerController.AllowInput = false;
            }
        }
    }

    void HandleBatState()
    {
        //set things to follow the ball
        //Turn Off MainCamera
        MainCamera.gameObject.SetActive(false);

        //Turn On OnHitCamera
        OnHitCamera.gameObject.SetActive(true);

        //if the ball is close to floor lets stop following
        if( Ball.transform.position.y <= 0.1f)
        {
            CurrentState = GameStates.ScoreState;
           
        }
    }

    void HandleStrikeState()
    {
        strikeTimer += Time.deltaTime;

        if (strikeTimer >= 2.0f)
        {
            batterOutTimer += Time.deltaTime;
            if (Strikes >= 3)
            {
                if (StartBatterOutTimer == false)
                {
                    StartBatterOutTimer = true;
                    UIController.SetText("Batter OUT!");
                    UIController.ToggleResultText(true);
                    batterOutTimer = 0.0f;
                }
            }
            else
            {
                strikeTimer = 0.0f;
                CurrentState = GameStates.PitchState;
                Ball = null;
            }
        }

        if(StartBatterOutTimer)
        {
            if (batterOutTimer >= 2.0f)
            {
                batterOutTimer = 0.0f;
                strikeTimer = 0.0f;
                CurrentState = GameStates.PitchState;
                Ball = null;
            }
        }
        
    }

    void HandleScoreState()
    {
        

        // wait two seconds looking at the ball and exit to pitch state
        scoreTimer += Time.deltaTime;
        if (scoreTimer >= 4.0f)
        {
            scoreTimer = 0.0f;
            CurrentState = GameStates.PitchState;

            Ball = null;
            //Turn On MainCamera
            MainCamera.gameObject.SetActive(true);

            //Turn Off OnHitCamera
            OnHitCamera.gameObject.SetActive(false);
            OnHitCamera.GetComponent<OnHitCamera>().SetTargetToFollow(null);

        }
        else if (scoreTimer >= 2.0f)
        {
            if (SecondEvaluationCompleted == false)
            {
                SecondEvaluationCompleted = true;
                if (FairScore)
                {
                    // show fair and score
                    UIController.SetText(string.Format("FAIR SCORE: {0}", NewScore));
                    UIController.ToggleResultText(true);
                }
                else
                {
                    // show foul
                    UIController.SetText("FOUL");
                    UIController.ToggleResultText(true);

                    //First two fouls are Strikes.
                    if (Strikes < 2)
                    {
                        IncreaseStrikeCount();
                    }
                }
            }

        }else 
        {
            RollBallStatus = Referee.RequestHitCall(Ball.transform);
            bool ballPastBase = Referee.IsPastBase(Ball.transform);
            bool enterFoul = EnterHitStatus == Referee.Hit_State.HIT_STATE_FOUL;
            bool rollFoul = RollBallStatus == Referee.Hit_State.HIT_STATE_FOUL;


            bool regularFoul = enterFoul && rollFoul;
            bool beforeBaseFoul = !ballPastBase && !enterFoul && rollFoul;
            bool pastBaseFoul = ballPastBase && enterFoul && !rollFoul;

            if( regularFoul || beforeBaseFoul || pastBaseFoul)
            {
                // its def a faul
                FairScore = false;
            }



        }

    }

    void HandleEndState()
    {
        endStateTimer += Time.deltaTime;
        if (endStateTimer >= 1.0f)
        {
            endStateTimer = 0.0f;
            //CurrentState = GameStates.PitchState;
            Ball = null;
        }
    }

    private void StartNewState()
    {
        switch (CurrentState)
        {
            case GameStates.PitchState:
                EnterPitchState();
                break;
            case GameStates.BatState:
                EnterBatState();
                break;
            case GameStates.StrikeState:
                EnterStrikeState();
                break;
            case GameStates.ScoreState:
                EnterScoreState();
                break;
            case GameStates.EndState:
                EnterEndState();
                break;
        };
    }

    void EnterPitchState()
    {
        Pitcher.HasCatcherCaughtBall = false;
        UIController.ToggleResultText(false);
        if (Strikes == 3)
        {
            Outs++;
            Strikes = 0;
        }

        if( Outs == 3)
        {
            CurrentState = GameStates.EndState;
        }

        //TODO: Convert to Display Image.
        Debug.Log(string.Format("Strikes : {0}  Outs : {1}", Strikes, Outs));
    }

    void EnterBatState()
    {
    }

    void EnterStrikeState()
    {
        IncreaseStrikeCount();
        UIController.SetText("Strike");
        UIController.ToggleResultText(true);
        StartBatterOutTimer = false;
    }

    void EnterScoreState()
    {
        SecondEvaluationCompleted = false;
        EnterHitStatus = Referee.RequestHitCall(Ball.transform);

        RollBallStatus = Referee.Hit_State.HIT_STATE_DEFAULT;

        FairScore = true;
        NewScore = (int)(Referee.GetBallMagnitude(Ball.transform));
        TotalScore += NewScore;
    }

    void EnterEndState()
    {
        UIController.SetText("Game Over");
        UIController.ToggleResultText(true);
    }

    private void IncreaseStrikeCount()
    {
        Strikes++;
    }
}
