﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficulty
{
    public enum Modes
    {
        DIFFICULTY_MODE_EASY = 0,
        DIFFICULTY_MODE_MEDIUM,
        DIFFICULTY_MODE_HARD,
    }
}
