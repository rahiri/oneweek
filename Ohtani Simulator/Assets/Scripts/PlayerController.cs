﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public Camera MainCamera;
    public Camera OnHitCamera;
    public GameObject BatContainer;

    public Pitcher Pitcher;
    private Bat Bat;

    Vector3 previousPos = new Vector3(0,0,0);

    [HideInInspector]
    public bool AllowInput;

	// Use this for initialization
	void Start ()
    {
        Bat = BatContainer.GetComponentInChildren<Bat>();
        Bat.OnBallHit += OnBallHit;

        Pitcher.OnPitcherThrow += OnPitcherThrow;

        AllowInput = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if( Input.GetMouseButtonDown(0))
        {
            Camera c = MainCamera;
            Vector3 mousePos = Input.mousePosition;
            Vector3 worldPos = c.ScreenToViewportPoint(new Vector3(mousePos.x,mousePos.y,c.nearClipPlane));
            previousPos = worldPos;

        }
        else
		if( Input.GetMouseButton(0))
        {
            Camera c = MainCamera;
            Vector3 mousePos = Input.mousePosition;
            
            Vector3 worldPos = c.ScreenToViewportPoint(new Vector3(mousePos.x,mousePos.y,c.nearClipPlane));
            
            Vector3 deltaPos = worldPos - previousPos;

            Vector3 batPos = BatContainer.transform.position;

            batPos.y += deltaPos.y;
            if (batPos.y < 0.4f)
                batPos.y = 0.4f;
            if (batPos.y > 1.2f)
                batPos.y = 1.2f;

            batPos.z += deltaPos.x;
            if (batPos.z < -0.3f)
                batPos.z = -0.3f;
            if (batPos.z > 0.3f)
                batPos.z = 0.3f;


            BatContainer.transform.position = batPos;

            previousPos = worldPos;
        }else
        if(Input.GetMouseButtonUp(0))
        {
            Bat.Play();
        }
        
	}

    private void OnBallHit(GameObject ball)
    {
        ////Debug.Log("Victor Ball Hit Event send!");
        ////Turn Off MainCamera
        //MainCamera.gameObject.SetActive(false);

        ////Turn On OnHitCamera
        //OnHitCamera.gameObject.SetActive(true);
        //OnHitCamera.GetComponent<OnHitCamera>().SetTargetToFollow(ball);
    }

    private void OnPitcherThrow()
    {
        //Debug.Log("Victor Ball Hit Event send!");
        ////Turn On MainCamera
        //MainCamera.gameObject.SetActive(true);

        ////Turn Off OnHitCamera
        //OnHitCamera.gameObject.SetActive(false);
        //OnHitCamera.GetComponent<OnHitCamera>().SetTargetToFollow(null);
    }

}
