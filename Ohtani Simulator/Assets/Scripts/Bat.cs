﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
    public float Mass = 1.0f;
    public float Velocity = 20.0f;

    // Delegates to notify
    public delegate void DelegateOnBallHit(GameObject ball);
    public event DelegateOnBallHit OnBallHit;

    Animator batAnimator
    {
        get { return GetComponent<Animator>(); }
    }

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
       

    }

    public void OnCollisionEnter(Collision collision)
    {
       if( collision.collider.name.Contains("Ball"))
        {
            collision.collider.attachedRigidbody.useGravity = true;
            Vector3 launchV = collision.contacts[0].normal;
            float milesToMeterFactor = 1609.34f /*Meter*/ / 1.0f /*Mile*// 3600.0f;


            float force = Force(Mass, Velocity, collision.collider.attachedRigidbody.mass, collision.collider.GetComponent<Ball>().Speed / milesToMeterFactor, Time.deltaTime);
            
            collision.collider.attachedRigidbody.AddForce(launchV * force);

            OnBallHit(collision.gameObject);
        }

    }

    float Force( float m1, float v1, float m2, float v2, float dt)
    {
        return (m1 * v1 - m2 * v2) / dt;
    }


    public void Play()
    {

        batAnimator.Play("Bat", 0);

    }

}
