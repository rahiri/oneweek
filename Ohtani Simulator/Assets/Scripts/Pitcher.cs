﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pitcher : MonoBehaviour
{
    

    public float DelayBetweenThrows;

    public float ThrowSpeed;

    public StrikeZone StrikeZone;

    public List<Ball> Balls;

    public Difficulty.Modes DifficultyMode;

    [HideInInspector]
    public bool HasCatcherCaughtBall;

    // Delegates to notify
    public delegate void DelegateOnPitcherThrow();
    public event DelegateOnPitcherThrow OnPitcherThrow;

    private float Timer = 0.0f;

    private Ball AvailableBall = null;

    [HideInInspector]
    public bool AllowPitch;

    // Use this for initialization
    void Start ()
    {
        AllowPitch = false;
		for(int index = 0; index < Balls.Count; ++index)
        {
            Balls[index].gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (AllowPitch)
        {
            ThrowBallRoutine();
        }
	}

    void ThrowBallRoutine()
    {
        Timer += Time.deltaTime;
        if (Timer >= DelayBetweenThrows)
        {
            SetupCurrentBall();
            ThrowCurrentBall();
            Timer = 0.0f;
        }
    }

    void SetupCurrentBall()
    {
        AvailableBall = null;
        for (int index = 0; index < Balls.Count; ++index)
        {
            if(Balls[index].Available)
            {
                AvailableBall = Balls[index];
                break;
            }
        }
        if (AvailableBall)
        {
            HasCatcherCaughtBall = false;
            Vector3 targetPosition = StrikeZone.GetTargetPosition(DifficultyMode);
            AvailableBall.Reset(transform.position, ThrowSpeed, targetPosition);
            AvailableBall.OnCatcherCatch -= OnCatcherCatch;
            AvailableBall.OnCatcherCatch += OnCatcherCatch;
            AvailableBall.gameObject.SetActive(true);
        }
    }

    void ThrowCurrentBall()
    {
        if(AvailableBall != null)
        {
            StrikeZone.ClearStaticBallImage();
            OnPitcherThrow();
            AvailableBall.Throw();
        }
    }

    private void OnCatcherCatch()
    {
        HasCatcherCaughtBall = true;
    }
}
