﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    // Delegates to notify
    public delegate void DelegateOnCatcherCatch();
    public event DelegateOnCatcherCatch OnCatcherCatch;

    [HideInInspector]
    public bool Available = true;

    [HideInInspector]
    public float Speed = 1.0f;

    private Vector3 TargetPosition;

    private Vector3 OriginalPosition;

    private Rigidbody RigidbodyComponent;

    // Use this for initialization
    void Start ()
    {
        Reset(transform.position, 1.0f, Vector3.zero);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //MovedToTarget();
	}

    public void Reset(Vector3 startPosition, float throwSpeed, Vector3 targetPos)
    {
        transform.position = startPosition;
        Speed = throwSpeed;
        TargetPosition = targetPos;
        OriginalPosition = transform.position;
        RigidbodyComponent = GetComponent<Rigidbody>();
        if (RigidbodyComponent)
        {
            RigidbodyComponent.useGravity = false;
        }
        SetupBallSpeedFromMPHToUnityUnits();
    }

    public void Throw()
    {
        Available = false;

        Vector3 targetPosition = TargetPosition;
        Vector3 directionVector = targetPosition - OriginalPosition;
        Vector3 speedVector;
        directionVector = directionVector.normalized;
        speedVector = directionVector * (Speed);

        RigidbodyComponent = GetComponent<Rigidbody>();
        if (RigidbodyComponent)
        {
            //RigidbodyComponent.AddForce(directionVector);
            RigidbodyComponent.velocity = speedVector;
        }

    }

    private void SetupBallSpeedFromMPHToUnityUnits()
    {
        //Convert from MPH to Unity Units atm assuming 1 Unity unit equals one feet.
        //5280 feet in a mile

        //float milesTofeetFactor = 5280.0f /*Feet*/ / 1.0f /*Mile*/;
        float milesToMeterFactor = 1609.34f /*Meter*/ / 1.0f /*Mile*/;

        //Current Speed in MPH
        Speed = (Speed * milesToMeterFactor) / 3600.0f; //This Result is in feet per second

    }

    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("entered");
        Available = true;

        if(collision.gameObject.name.Equals("Catcher"))
        {
            if (RigidbodyComponent)
            {
                //RigidbodyComponent.AddForce(directionVector);
                RigidbodyComponent.velocity = Vector3.zero;
            }

            OnCatcherCatch();
        }

    }

}
