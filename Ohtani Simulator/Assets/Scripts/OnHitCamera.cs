﻿using UnityEngine;
using System.Collections;

public class OnHitCamera : MonoBehaviour
{

    private Vector3 PositionOffset;

    private GameObject TargetToFollow;

    void Start()
    {
        PositionOffset = new Vector3(7.0f, 5.0f, 0.0f);
    }

    void LateUpdate()
    {
        if (TargetToFollow)
        {
            // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
            transform.position = TargetToFollow.transform.position + PositionOffset;
            transform.LookAt(TargetToFollow.transform);
        }
    }

    public void SetTargetToFollow(GameObject targetObject)
    {
        TargetToFollow = targetObject;
    }
}